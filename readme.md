# Práctica 2 - HIVE


# Beeline

```bash
beeline -u jdbc:hive2://hive:10000/default
```
## 1. Creación de bases de datos/schemas

### 1.1 Creación en la capa staging
```sql
CREATE DATABASE IF NOT EXISTS st_operations
COMMENT "Staging Database"
LOCATION "/curso/staging/";
```

### 1.2 Creación en la capa Raw
```sql
CREATE DATABASE IF NOT EXISTS r_operations
COMMENT "Raw Database of operations"
LOCATION "/curso/raw/operations";
```

### 1.3 Creación en la capa de Master
```sql
CREATE DATABASE IF NOT EXISTS m_operations
COMMENT "Master Database of operations"
LOCATION "/curso/master/operations";

```
## 2. Creación de tablas
### 2.2 Creación de tabla Raw

En la capa de raw, se matienen los datos sin realizar transformaciones sobre ellos, en formato string, únicamente se tabulan y se particionan por la fecha de ingesta.También se suelen incluir datos de auditoria.

```sql
CREATE EXTERNAL TABLE IF NOT EXISTS r_operations.t_loan(
  loan_id STRING COMMENT "identification of the account",
  account_id STRING COMMENT "identification of the account",
  `date` STRING COMMENT "date when the loan was granted",
  amount STRING COMMENT "amount of money",
  duration STRING COMMENT "duration of the loan",
  payments STRING COMMENT "monthly payments",
  status STRING COMMENT "status of paying off the loan")
  COMMENT "This table contains the nformation about loans"
partitioned by (indate string)
STORED AS AVRO;
```
### 2.2 Creación de tabla Master

Las tablas de master se crean con el tipo de dato adecuado, particiondas por algún dato de negocio. Normalmente en formato columnar(parquet, orc)

```sql
CREATE EXTERNAL TABLE IF NOT EXISTS m_operations.t_loan(
  loan_id INT COMMENT "identification of the account",
  account_id INT COMMENT "identification of the account",
  `date` TIMESTAMP COMMENT "date when the loan was granted",
  amount DECIMAL(10,2) COMMENT "amount of money",
  duration INT COMMENT "duration of the loan",
  payments DECIMAL(10,2) COMMENT "monthly payments"
  ) COMMENT "This table contains the information about loans"
partitioned by (year INT, month INT, status STRING)
STORED AS PARQUET TBLPROPERTIES ("parquet.compression"="SNAPPY");
```


## 3. Ingesta de datos

Aunque la ingesta se va a realiar a lo largo del curso con procesos de Spark, para esta práctica vamos a realizar una ingesta con Hive. Este método nos puede ser en el futuro muy útil para cargar datos de pruebas en entornos previos.


### 3.1 Creación de tabla staging
```sql
CREATE EXTERNAL TABLE IF NOT EXISTS st_operations.t_loan (
    loan_id STRING ,
    account_id STRING ,
    `date` STRING,
    amount STRING,
    duration STRING,
    payments STRING,
    status STRING
    )
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
STORED AS TEXTFILE
LOCATION '/curso/staging/loans'
tblproperties ("skip.header.line.count"="1");
```

### 4 Ingesta

Vamos a realizar la ingesta con spark, para ello debemos abrir un notebook 

```scala
spark.conf.set("hive.exec.dynamic.partition.mode", "nonstrict")
spark.conf.set("spark.hadoop.hive.exec.dynamic.partition", "true")
spark
.table("st_operations.t_loan")
.withColumn("indate",lit("20210525"))
.write
.insertInto("r_operations.t_loan")
```


## 5 Operaciones Con Particiones

### 5.1 Mostrar Particiones
```sql
show partitions r_operations.t_loan
```

### 5.2 Borrar partición
```sql
ALTER TABLE r_operations.t_loan DROP IF EXISTS PARTITION(indate="20200428")
```
Si la tabla es EXTERNAL no se borran los datos de la partición y será necesario borrarla desde hdfs

**No ejecutar para el ejecicio**
```sql
hdfs dfs -rm -R /raw/operations/t_loan/indate=20200428
```


### 5.3 Reparar o añadir una nueva partición

Si se ha borrado una partición de una tabla externa o se a añadido una nueva de forma manual(directamente desde hdfs) para que hive refresque la metadata tendremos que ejecutar el siguiente comando:

```sql
msck repair table r_operations.t_loan
```

## Referencias

[Hive Home](https://cwiki.apache.org/confluence/display/Hive/Home#Home-HiveDocumentation)

## Ejercicios
[Ejercicios](https://bitbucket.org/gabriggr/dataset/src/master/practica_2/ejercicios.md)
