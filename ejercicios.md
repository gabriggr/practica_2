# Práctica 2 - HIVE - Ejercicios

La información de los ficheros la tenmos en el siguiente enlace
[dataset info](https://bitbucket.org/gabriggr/practica_1/src/master/datasetInfo.xlsx) , para descarga en ".../open raw"


## 0. Creación de todas la tablas en staging, 
* La localización será la de los directorios subidos a staging en la primera práctica. ej /curso/staging/loans
### 0.1 Nombre de las tablas
  * Loans : t_loan
  * clients: t_client
  * accounts: t_account
  * transactions: t_transaction
  * orders: t_order
  * district: t_district
  * cards: t_card
  * disps: t_disp

## 1. Creación de todas las tablas en raw
* Todas las tablas guardarán los ficheros en AVRO
* Todas las tablas tienen que ser EXTERNAL
* Todos los campos tienen que ser de tipo String
* Todas las tablas tienen que estar particionadas por la fecha de carga **"indate"**

### 1.1 Nombre de Tablas

* Loans : t_loan
* clients: t_client
* accounts: t_account
* transactions: t_transaction
* orders: t_order
* district: t_district
* cards: t_card
* disps: t_disp

## 2. Creación de las tablas en master
* Los tipos de datos están definidos en el fichero datasetInfo.xlsx
* Todas las tablas tienen que ser EXTERNAL
* Todas las tablas guardarán los ficheros en PARQUET y SNAPPY

***Por incompatibilidad con parquet para los tipos DATE utilizaremos TIMESTAMP***

### 2.1 Nombre de Tablas

* Loans : t_loan
* clients: t_client
* accounts: t_account
* transactions: t_transaction
* orders: t_order
* district: t_district
* cards: t_card
* disps: t_disp

### 2.2 Particiones
1. transactions:
   - year: INT
   - month: INT
   - type : STRING
2. clients: No particionada
3. accounts: No particionada
4. orders: No particionada
5. districts: No particionada
6. cards:
   - type: STRING
7. loans:
   - year: INT
   - month: INT
   - type : STRING
8. disp: No particionada

***Los campos que se definen en la partición no se tienen que definir en los campos de la tabla, solo como campos de partición.***

### 3 Ingesta Staging to Raw

Utilizaremos spark como hicimos en el caso práctico de la sesión 2.
